window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("topnav").style.padding = "0px 10px 0px 10px";
    document.getElementById("logo").style.fontSize = "25px";
  } else {
    document.getElementById("topnav").style.padding = "0px 10px 0px 10px";
    document.getElementById("logo").style.fontSize = "35px";
  }
}
var QuotesArray = ["“Therefore my heart is glad, and my glory rejoices;<br> my flesh also will rest in hope.”<br> ~ Psalm 16:9",
"“You are my hiding place and my shield; I hope in Your word.”<br> ~ Psalm 119:114",
"“O Israel, hope in the LORD; for with the LORD there is mercy,<br> and with Him is abundant redemption.”<br> ~ Psalm 147:11",
"“For surely there is a hereafter, and your hope will not be cut off.”<br> ~ Proverbs 23:18",
"“Blessed is the man who trusts in the LORD,<br> and whose hope is the LORD.” <br> ~ Jeremiah 17:7",
"“‘The LORD is my portion,’ says my soul, ‘therefore I hope in Him!’”<br> ~ Lamentations 3:24",
"“And everyone who has this hope in Him purifies himself, just as He is pure.”<br> ~ 1 John 3:3",
"“Looking for the blessed hope and <br> glorious appearing of our great God and Savior Jesus Christ.”<br> ~ Titus 2:13",
"“Rejoicing in hope, patient in tribulation, continuing steadfastly in prayer.”<br> ~Romans 12:12",
]

function ChangeQuote(){
  console.log("function cALLEd")
  var Quote = QuotesArray[Math.floor(Math.random()*QuotesArray.length)]
  console.log(Quote)
  let element = document.getElementById('ver')
  element.innerHTML = Quote
}